# Learning many aspects from Spaceflight mechanic over Satellite design to Python coding and DevOps

This repository existis to tech you the necessary pratictices in 
- AI
- software engineering
- Python

Use everything shown in this repo as examples, from which you can learn. 

# Academic

- ESA Scientific Readiness Level [pdf](https://esamultimedia.esa.int/docs/EarthObservation/Scientific-Readiness-Levels-SRL-Handbook-version-2.0-10-February-2023.pdf)
- Papers we love and sources for papers [GitHub](https://github.com/papers-we-love/papers-we-love)

## Requirements
Befor we can starting building something useful, we have to understand, what actually will be used.
Meaning, we have to identify the project's requirements.

| **ID** | **Requirement**                          | **Description**                                             | **Priority** | **Validation Method**  | Acceptance Criteria |
|--------|------------------------------------------|------------------------------------------------------------|--------------|------------------------------|--------|
| R-001  | Satellite Mass                           | Must not exceed 50 kg                                       | High         | Weighing                             |
| R-002  | Operational Lifetime                     | Minimum operational lifetime of 2 years                     | High         | Durability tests                     |
| R-003  | Communication Range                      | At least 500 km for reliable data transmission             | Medium       | Range tests                          |
| R-004  | Power System                             | Solar panels should generate at least 100W                  | High         | Output measurement                   |
| R-005  | Attitude Determination and Control System| Accuracy within 0.1 degree                                  | Medium       | Calibration tests                    |
| R-006  | Thermal Control                          | Must operate between -20°C and 50°C                         | High         | Temperature chamber tests            |
| R-007  | Data Handling                            | Store at least 1 GB of data                                 | Medium       | Data storage and retrieval tests     |
| R-008  | Structural Integrity                     | Survive vibration loads up to 20 g                          | High         | Vibration and structural tests       |
| R-009  | Launch Compatibility                     | Compatible with Falcon 9 launch vehicle                     | Low          | Fit check and adapter tests          |


## Verification, Validation
- Verification vs Validation [Wikipedia](https://en.wikipedia.org/wiki/Verification_and_validation)
  - Verification: internetal process
  - Validation: incoperating outside world
- Validation: Plausability Check
- Verification: Mathematical Proof

### According to ChatGPT 1o-Preview
| **Aspect**     | **Validation**                                                                                                           | **Verification**                                                                                           | **Evaluation**                                                                                             |
|----------------|--------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------|
| **Definition** | Process of checking whether the product meets the user's needs and requirements                                          | Process of checking whether the product meets the specified design and technical specifications             | Process of assessing the overall performance, quality, or impact of the product                             |
| **Purpose**    | Ensure the right product is built                                                                                        | Ensure the product is built right                                                                           | Determine the value or effectiveness of the product                                                         |
| **Timing**     | Typically performed after verification, often in the final stages or after completion                                    | Performed throughout development, especially during implementation                                           | Can be conducted at various stages, including after deployment                                              |
| **Methods**    | User acceptance testing, field testing, usability testing                                                                | Inspections, reviews, walkthroughs, code analysis, unit testing                                              | Performance testing, benchmarking, user feedback, surveys                                                   |
| **Outcome**    | Confirmation that the product satisfies user needs                                                                       | Confirmation that the product meets design specifications                                                    | Assessment of how well the product performs in real-world scenarios                                         |
| **Examples**   | Testing software to ensure it solves the user's problem; ensuring a medical device improves patient outcomes             | Checking code against design documents; ensuring hardware components meet technical standards               | Measuring user satisfaction after product release; analyzing the impact of a policy change on performance   |



## Methodical Working
- Building a Work Breakdown Structure (WBS) [PlantUML](https://plantuml.com/wbs-diagram)
- **FMEA** - [Wikipedia](https://en.wikipedia.org/wiki/Failure_mode_and_effects_analysis) \
Purpose: Handling customer requirements
- **House of Quality** - [Wikipedia](https://en.wikipedia.org/wiki/Quality_function_deployment) \
Purpose: Handling customer requirements
- **SWOT** - [Wikipedia](https://en.wikipedia.org/wiki/SWOT_analysis) \
Purpose: task management

- **Lastenheft & Pflichtenheft**

| Feature | Lastenheft | Pflichtenheft |
|---------|------------|--------------|
| **Focus** | Defines WHAT needs to be done | Defines HOW to do it |
| **Purpose** | Describes requirements & objectives | Describes technical implementations |
| **Created by** | Client or Customer | Supplier or Contractor |
| **Detail Level** | High-level, more general | Detailed, technical specifics |
| **Use** | Basis for Pflichtenheft | Basis for implementation & project planning |

- Morphological Box
- FORDEC [Wikipedia](https://de.wikipedia.org/wiki/FORDEC?wprov%253Dsfti1)
- Challenge-based Learning (CBL)
  - 

- Levels of System Architectures acording to ChatGPT

| **System Architecture** | **Explanation**|
|-------------|-------------|
| **Functional Architecture**  | Describes the system in terms of its functions, focusing on *what* the system must do. It defines the activities, tasks, and processes to be performed by the system and how they interact. |
| **Logical Architecture**     | Represents the system in terms of logical components and their relationships, focusing on *how* the system will fulfill the required functions. It breaks down the system into components or modules that are logically related but not yet physically implemented. |
| **Physical Architecture**    | Specifies the physical elements of the system, such as hardware and infrastructure. It describes *where* and *what* specific components are used, including materials, sizes, and configurations. |
| **Technical Architecture**   | Focuses on the technical details of the system, including software, hardware platforms, and communication protocols. It often involves decisions about tools, technologies, and systems integration. |
| **Modular Architecture**     | Describes a system built from independent, interchangeable components or modules, each performing a specific function. This architecture allows for easy upgrades and adaptability, often used in CubeSat and other spacecraft designs. |
| **Distributed Architecture** | Represents systems where components are spread across multiple locations or platforms but work together to achieve the overall goal. This architecture is often used in satellite constellations and multi-spacecraft missions. |
| **Layered Architecture**     | Organizes the system into hierarchical layers, where each layer has specific responsibilities. This approach separates concerns, making the system easier to manage, develop, and scale. It's common in both software and hardware system design. |
| **Client-Server Architecture**| Consists of two main parts: clients that request resources or services and servers that provide them. This architecture is common in ground-based satellite operations where the satellite (client) communicates with control centers (servers). |
| **Event-Driven Architecture**| Focuses on producing and responding to events or triggers. It’s useful for real-time space operations where specific events (e.g., sensor readings, command signals) drive system behavior. |
| **Service-Oriented Architecture (SOA)** | Comprises loosely coupled services that interact to provide system functionality. Each service is independent and reusable, making it flexible and adaptable, particularly in ground control systems or mission planning software. |



# Ingredients
- CI
  - tox
  - flake8
  - pytest failure
-  local development
  - tox
  - unit testing 
    - [pytest tips and tricks - pythontest.com](https://pythontest.com/pytest-tips-tricks/)
- git [YouTube/Valentin Despa](https://www.youtube.com/watch?v=4lxvVj7wlZw)
  - issues 
  - milestones
- docker
  - docker environment file [YouTube/anthonyexplains](https://youtu.be/kL0q-7alfQA)

# Streamlit
- Cool Grid Layout [streamlit-extras](https://arnaudmiribel.github.io/streamlit-extras/extras/grid/)
- Cool Stats [streamlit-extras](https://arnaudmiribel.github.io/streamlit-extras/extras/metric_cards/)
- Streamlit Tags [GitHub](https://github.com/gagan3012/streamlit-tags)
- :star: [Streamlit Components - Community Tracker](https://discuss.streamlit.io/t/streamlit-components-community-tracker/4634)
- :star: [Important Streamlit Components to use - YouTube](https://www.youtube.com/watch?v=_Um12_OlGgw)
- Streamlit Searchbox [GitHub](https://github.com/m-wrzr/streamlit-searchbox)
- :star: Presentation Slides in Streamlit [Streamlit Demo](https://bouzidanas-streamlit-reveal-slidesexamplesmarkdown-demo-cindcb.streamlit.app/)
- Highcharts Diagram for Streamlit [Streamlit Demo](https://aalteirac-streamlit-highcharts-test-app-main-3vgde6.streamlit.app/)

# Python
- Python Tips [dev.to]()
- [Python Graph Library](https://python-graph-gallery.com/)

# docker
- Docker-Compose Spec [official GitHub Docu](https://github.com/compose-spec/compose-spec/blob/master/spec.md)

# AI
- FastAI open book [GitHub](https://github.com/fastai/fastbook)
- onnx Open standard for machine learning interoperability - [GitHub](https://github.com/onnx/onnx)
- Reinforcement Learning [AppliedRL- netlify](https://applied-rl-course.netlify.app/en/module4)
- What Is Artificial Intelligence? Definition, Uses, and Types [Coursera](https://www.coursera.org/articles/what-is-artificial-intelligence)
- RL algo library [ray docu](https://docs.ray.io/en/latest/rllib/index.html)
- q-transformer [GitHub Pages](https://qtransformer.github.io/)
- Physics Simulations with DL [physicsbaseddeeplearning](https://physicsbaseddeeplearning.org/intro.html#)

# Databases
- [Database Terminology - A Dictionary of the Top Database Terms](https://raima.com/database-terminology/)
- [What is a vector database? IBM](https://www.ibm.com/topics/vector-database)

# AI in Space
- [Using Artificial Intelligence for Space Challenges: A Survey, Russo and Lax](https://www.mdpi.com/2076-3417/12/10/5106)

# Space Tech
- [Satellite Operations](http://phxcubesat.asu.edu/ConOps)
- [NASA's Lessons Learned Library](https://llis.nasa.gov/)
- [NASA Technology Taxonomy](https://ntrs.nasa.gov/api/citations/20200000399/downloads/20200000399.pdf)
- [NASA State of the Art Space Technology CubeSat](https://www.nasa.gov/smallsat-institute/sst-soa/power-subsystems/)
- [Chronology of Space Launches](https://space.skyrocket.de/directories/chronology.htm)
- [ESA Academy - Basic Steps in Designing a Space Mission](https://swe.ssa.esa.int/TECEES/spweather/Alpbach2002/Marsden-basic%20steps%20in%20designing%20a%20space%20mission.pdf)

# Space Missions
- Satellite Missions catalogue [eoPortal](https://www.eoportal.org/satellite-missions)
- The CEOS Database [eoHandbook](https://database.eohandbook.com/database/missiontable.aspx)
  Searchable Table of Earth Observation Satellite Missions

# Concurrent Engineering (CE)
- 10 Success Factors for Concurrent Design [RHEA](https://www.rheagroup.com/10-success-factors-for-concurrent-design/)

# MBSE
- MBSE at ESA [ESA](https://www.esa.int/Enabling_Support/Space_Engineering_Technology/Shaping_the_Future/Goodbye_Documents_Hello_Models_A_Model-based_Approach_to_System_Engineering?hss_channel=lcp-41230)
- AI in Model-based Systems Engineering [SE Live](https://www.selive.de/ai-in-mbse/)
  - List of Identified AI Use Cases in MBSE


# SysMLv2
- [Presentation by ARA](https://www.ncsi.com/wp-content/uploads/2023/05/1400-ARA-Creating-Integrated-Digital-Environments-with-SysML-v2.pdf)
- [Overview Presentation by OMG](https://www.omg.org/pdf/SysML-v2-Overview.pdf)
- GER: [SysML v2 Referenzimplementierung: Ein Erfahrungsbericht](https://www.se-trends.de/sysml-v2-referenzimplementierung/?no_cache=1687413810)

# CubeSat
- CubeSat Standards [ISO](https://www.iso.org/standard/60496.html)
- Power Budget [CalPoly](http://mstl.atl.calpoly.edu/~workshop/archive/2011/Spring/Day%203/1610%20-%20Clark%20-%20Power%20Budgets%20for%20CubeSat%20Mission%20Success.pdf)
- [10y CubeSats @ ESA](https://www.esa.int/ESA_Multimedia/Images/2023/07/Infographic_ESA_Technology_CubeSats_the_first_10_years)
- Small Spacecraft Systems Virtual Institute [NASA](https://www.nasa.gov/smallsat-institute/)

# Courses
- [Space System Verification and Validation, NASA appel](https://appel.nasa.gov/course-catalog/appel-ssvv/)
- MBSE [Coursera](https://www.coursera.org/learn/mbse)

# Related Projects
- Simcenter Studio [siemens](https://plm.sw.siemens.com/en-US/simcenter/integration-solutions/studio/)
- Morpheus Space
- [Altrair Hyperworks]https://altair.com/altair-hyperworks)

---
---
# AI4CE Glossary
| Term | Description |
| ------ | ------ |
|   Design Space     |   The complete set of all possible component combinations within a given database of components    |
|Deep Reinforcement Learning| DRL, Reinforcement Learning that utilises Neural Networks to approximate its internal reward function|
|eFunction|The evaluation Function is a mathematical formular, with which the reward for each component can be calculated --> 01_CSSysArch |
|Reward|The reward is a numerical score that is calculated for a specific component, based on the eFunction and System&Orbit requirements |
|Sub-System|Building blocks out of which a system is build. A sub-system can function on its own, but only makes sense in the context of the rest of the system. eg: Camera Module, Power Module, |
|   System    | A system is an entity that consits of more than one part/sub-system. Its functionality is greater than the sum of each part.eg CubeSat, Rover, Drone, Tiny House ..    |
|System Generator| A method that outputs a list of components, that together form a complete system. AI4CE conducts research on comparing the effectivness on differen generator methods, to measure in which scenario AI generators perform how well in comparison to other non-AI ones. Currently implemented: DRLs, brute force, random
