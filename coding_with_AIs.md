# First version of the Main Concept of the Project
As a busy engineer, I want a todo app, that helps me to keep track of my tasks, projects and progress in project and life, so that I can easily monitor my life.
Inspired by role-playing games (Pokemon, Cyberpunk2077, ..) I would like to have a dashboard-like interface, that shows my progress in plots, diagrams and other UI elements like progress bars and alike. 
The idea is to use gamification, where I (the user) is a character with stats and levels. The "game charakter" can improve, if the real user does its tasks and similar aspects.

Please layout a plan for building the proposed project idea. It has to include these additional constraints, which always must be considered for the whole project:
- Development MUST follow a TDD approach, please adopt the backlog
- We need a backlog.md to store the backlog and keep track of the userstories and acceptance criterias.
- And we need a README.md for general project documentation
- Please add checkboxes so that stories can be marked as done. Mark tasks as done, if possible.
- Please add to the backlog the purpose of this product
- please keep the backlog and readme always up to date
- Write the most important stories to created a backlog, prioritise them as a list and put them in a backlog.md file
- when creating sprints and tasks, always organise them into vertical slices of the MVP
- the app will need to be a progressive web app in the end, to also enable great mobile-compatability


Please create a story map for this product
And please keep the minimal stories for a MVP

And then:
- Write the most important stories to created a backlog, prioritise them as a list and put them in a backlog.md file
- create Sprints and add them to the backlog


# Fixing code with AI, based on TDD
- Please run the test Suite and then describe the Errors if Tests fail.
Please look for Logical and structural issues in he Setup of Tests first. The test should be designed slightly flexible. Do not try to fix the errors yet. 
- Please layout a plan to fix these issues and please prioritise for the Impact of the fix. Please Remember, that a simpler solution is often the better one. Do not try to fix the errors yet. 
- please check if the Tests and your Approach to fix the bad Tests are Independent. That is the power of unit tests. Do not try to fix the errors yet.
- Now start to fix the issues one after the other. Start with the fix with the highest Impact and run the Tests again. if the test is still not fixed, please try again by Fixing it and running the test again. Please Remember, that a simpler solution is often the better one.
