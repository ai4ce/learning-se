# Final Presentaion
- What was the work you did on your own, and what was given?
- What was your project about? in 2 sentences

## Validation
- How did you validate your claim?
- What isn't covered by your validation?
- Did you use some use cases? What are they?

## Research Methodology
- What was your research question?
- What did you do your research on? 
